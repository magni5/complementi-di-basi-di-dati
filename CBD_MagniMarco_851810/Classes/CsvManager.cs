﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;
using CsvHelper;
using CsvHelper.Configuration;

namespace CBD_MagniMarco_851810.Classes
{
    public class CsvManager
    {
        MongoClient client;
        string csvFolderPath;

        public CsvManager(MongoClient c, string cfp)
        {
            client = c;
            csvFolderPath = cfp;
        }

        public void Elabora()
        {
            ImportaReference();
            ImportaEmbedded();
        }

        void ImportaReference()
        {
            client.DropDatabase("esd-reference");
            IMongoDatabase dbReference = client.GetDatabase("esd-reference");
            //Console.WriteLine("Mi sono connesso al database esd-reference");

            Console.WriteLine("\nCsvManager: inizio importazione esd-reference");
            Stopwatch watch = Stopwatch.StartNew();

            var configuration = new CsvConfiguration(System.Globalization.CultureInfo.InvariantCulture)
            {
                Encoding = System.Text.Encoding.UTF8,
                Delimiter = ";",
                HasHeaderRecord = true,
                IgnoreBlankLines = true
            };

            using (var reader = new StreamReader(csvFolderPath + "Team.csv"))
            using (var csv = new CsvReader(reader, configuration))
            {
                List<BsonDocument> teamsToInsert = new List<BsonDocument>();
                csv.Read();
                csv.ReadHeader();
                while (csv.Read())
                {
                    BsonDocument t = new BsonDocument();
                    t["code"] = csv.GetField<int>("code");
                    t["short_name"] = csv.GetField("team_short_name");
                    t["long_name"] = csv.GetField("team_long_name");
                    BsonDocument attributes = new BsonDocument();
                    attributes["buildUpPlaySpeed"] = csv.GetField("buildUpPlaySpeed");
                    attributes["buildUpPlayDribbling"] = csv.GetField("buildUpPlayDribbling");
                    attributes["buildUpPlayPassing"] = csv.GetField("buildUpPlayPassing");
                    attributes["buildUpPlayPositioning"] = csv.GetField("buildUpPlayPositioning");
                    attributes["chanceCreationPassing"] = csv.GetField("chanceCreationPassing");
                    attributes["chanceCreationCrossing"] = csv.GetField("chanceCreationCrossing");
                    attributes["chanceCreationShooting"] = csv.GetField("chanceCreationShooting");
                    attributes["chanceCreationPositioning"] = csv.GetField("chanceCreationPositioning");
                    attributes["defencePressure"] = csv.GetField("defencePressure");
                    attributes["defenceAggression"] = csv.GetField("defenceAggression");
                    attributes["defenceTeamWidth"] = csv.GetField("defenceTeamWidth");
                    attributes["defenceDefenderLine"] = csv.GetField("defenceDefenderLine");
                    t["attributes"] = attributes;
                    teamsToInsert.Add(t);
                }
                dbReference.GetCollection<BsonDocument>("Team").InsertMany(teamsToInsert);
            }

            using (var reader = new StreamReader(csvFolderPath + "Match.csv"))
            using (var csv = new CsvReader(reader, configuration))
            {
                List<BsonDocument> matchesToInsert = new List<BsonDocument>();
                csv.Read();
                csv.ReadHeader();
                while (csv.Read())
                {
                    BsonDocument m = new BsonDocument();
                    BsonDocument league = new BsonDocument();
                    league["name"] = csv.GetField("league");
                    league["country"] = csv.GetField("country");
                    m["league"] = league;
                    m["season"] = csv.GetField("season");
                    m["stage"] = csv.GetField<int>("stage");
                    m["date"] = csv.GetField<DateTime>("date").ToLocalTime();

                    foreach (string x in new string[] { "home", "away" })
                    {
                        BsonDocument t = new BsonDocument();
                        t["team_code"] = csv.GetField<int>(x + "_team_code");
                        t["goal"] = csv.GetField<int>(x + "_team_goal");
                        t["formation"] = csv.GetField(x + "_formation");
                        BsonArray players = new BsonArray(11);
                        for (int i = 1; i <= 11; i++)
                            players.Add(csv.GetField(x + "_player_" + i));
                        t["players"] = players;
                        m[x] = t;
                    }
                    matchesToInsert.Add(m);
                }
                dbReference.GetCollection<BsonDocument>("Match").InsertMany(matchesToInsert);
            }

            watch.Stop();
            long elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("CsvManager: fine importazione esd-reference\nTempo(ms): " + elapsedMs);
        }

        void ImportaEmbedded()
        {
            client.DropDatabase("esd-embedded");
            IMongoDatabase dbEmbedded = client.GetDatabase("esd-embedded");
            //Console.WriteLine("Mi sono connesso al database esd-embedded");

            Console.WriteLine("\nCsvManager: inizio importazione esd-embedded");
            Stopwatch watch = Stopwatch.StartNew();

            var configuration = new CsvConfiguration(System.Globalization.CultureInfo.InvariantCulture)
            {
                Encoding = System.Text.Encoding.UTF8,
                Delimiter = ";",
                HasHeaderRecord = true,
                IgnoreBlankLines = true
            };

            using (var reader = new StreamReader(csvFolderPath + "Match.csv"))
            using (var csv = new CsvReader(reader, configuration))
            {
                List<BsonDocument> matchesToInsert = new List<BsonDocument>();
                csv.Read();
                csv.ReadHeader();
                while (csv.Read())
                {
                    BsonDocument m = new BsonDocument();
                    BsonDocument league = new BsonDocument();
                    league["name"] = csv.GetField("league");
                    league["country"] = csv.GetField("country");
                    m["league"] = league;
                    m["season"] = csv.GetField("season");
                    m["stage"] = csv.GetField<int>("stage");
                    m["date"] = csv.GetField<DateTime>("date").ToLocalTime();

                    foreach (string x in new string[] { "home", "away" })
                    {
                        BsonDocument t = new BsonDocument();
                        t["team_code"] = csv.GetField<int>(x + "_team_code");
                        t["team"] = "";
                        t["goal"] = csv.GetField<int>(x + "_team_goal");
                        t["formation"] = csv.GetField(x + "_formation");
                        BsonArray players = new BsonArray(11);
                        for (int i = 1; i <= 11; i++)
                            players.Add(csv.GetField(x + "_player_" + i));
                        t["players"] = players;
                        m[x] = t;
                    }
                    matchesToInsert.Add(m);
                }
                dbEmbedded.GetCollection<BsonDocument>("Match").InsertMany(matchesToInsert);
            }

            using (var reader = new StreamReader(csvFolderPath + "Team.csv"))
            using (var csv = new CsvReader(reader, configuration))
            {
                csv.Read();
                csv.ReadHeader();
                while (csv.Read())
                {
                    BsonDocument t = new BsonDocument();
                    t["code"] = csv.GetField<int>("code");
                    t["short_name"] = csv.GetField("team_short_name");
                    t["long_name"] = csv.GetField("team_long_name");
                    BsonDocument attributes = new BsonDocument();
                    attributes["buildUpPlaySpeed"] = csv.GetField("buildUpPlaySpeed");
                    attributes["buildUpPlayDribbling"] = csv.GetField("buildUpPlayDribbling");
                    attributes["buildUpPlayPassing"] = csv.GetField("buildUpPlayPassing");
                    attributes["buildUpPlayPositioning"] = csv.GetField("buildUpPlayPositioning");
                    attributes["chanceCreationPassing"] = csv.GetField("chanceCreationPassing");
                    attributes["chanceCreationCrossing"] = csv.GetField("chanceCreationCrossing");
                    attributes["chanceCreationShooting"] = csv.GetField("chanceCreationShooting");
                    attributes["chanceCreationPositioning"] = csv.GetField("chanceCreationPositioning");
                    attributes["defencePressure"] = csv.GetField("defencePressure");
                    attributes["defenceAggression"] = csv.GetField("defenceAggression");
                    attributes["defenceTeamWidth"] = csv.GetField("defenceTeamWidth");
                    attributes["defenceDefenderLine"] = csv.GetField("defenceDefenderLine");
                    t["attributes"] = attributes;

                    dbEmbedded.GetCollection<BsonDocument>("Match").UpdateMany("{ 'home.team_code': " + csv.GetField<int>("code") + " }", "{ $set: { 'home.team': " + t + " }, $unset: { 'home.team_code': '' } }");
                    dbEmbedded.GetCollection<BsonDocument>("Match").UpdateMany("{ 'away.team_code': " + csv.GetField<int>("code") + " }", "{ $set: { 'away.team': " + t + " }, $unset: { 'away.team_code': '' } }");
                }
            }

            watch.Stop();
            long elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("CsvManager: fine importazione esd-embedded\nTempo(ms): " + elapsedMs);
        }
    }
}