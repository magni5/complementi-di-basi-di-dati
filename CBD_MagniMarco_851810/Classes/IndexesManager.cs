﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;

namespace CBD_MagniMarco_851810.Classes
{
    public class IndexesManager
    {
        MongoClient client;
        IMongoDatabase dbReference;
        IMongoDatabase dbEmbedded;
        List<Indice> indici;

        public IndexesManager(MongoClient c)
        {
            client = c;
            dbReference = client.GetDatabase("esd-reference");
            dbEmbedded = client.GetDatabase("esd-embedded");

            InizializzaIndici();
        }

        void InizializzaIndici()
        {
            indici = new List<Indice>();

            indici.Add(new Indice("Reference", "Match", "league.name"));
            indici.Add(new Indice("Embedded", "Match", "league.name"));

            indici.Add(new Indice("Reference", "Team", "code"));
            indici.Add(new Indice("Reference", "Match", "home.team_code"));
            indici.Add(new Indice("Reference", "Match", "away.team_code"));
            indici.Add(new Indice("Embedded", "Match", "home.team.code"));
            indici.Add(new Indice("Embedded", "Match", "away.team.code"));

            indici.Add(new Indice("Reference", "Team", "long_name"));
            indici.Add(new Indice("Embedded", "Match", "home.team.long_name"));
            indici.Add(new Indice("Embedded", "Match", "away.team.long_name"));

            indici.Add(new Indice("Reference", "Team", "attributes.buildUpPlayPassing"));
            indici.Add(new Indice("Embedded", "Match", "home.team.attributes.buildUpPlayPassing"));
            indici.Add(new Indice("Embedded", "Match", "away.team.attributes.buildUpPlayPassing"));
        }

        public void Elabora()
        {
            bool uscita = false;
            do
            {
                Console.WriteLine("\nGestione indici\n0) Torna indietro\n1) Stato indici\n2) Elenco indici\n3) Crea indici\n4) Elimina indici");
                Console.Write("Scelta: ");
                string input = Console.ReadLine();

                switch (input)
                {
                    case "0":
                        uscita = true;
                        break;
                    case "1":
                        StampaStatoIndici();
                        break;
                    case "2":
                        StampaElencoIndici();
                        break;
                    case "3":
                        CreaIndici();
                        break;
                    case "4":
                        EliminaIndici();
                        break;
                    default:
                        Console.WriteLine("Scelta non valida");
                        break;
                }
            }
            while (!uscita);
        }

        void StampaStatoIndici()
        {
            List<BsonDocument> ind = dbReference.GetCollection<BsonDocument>("Match").Indexes.List().ToList();
            Console.WriteLine("\n" + (ind.Count > 1 ? "Indici attivi" : "Indici non attivi"));
        }

        void StampaElencoIndici()
        {
            Console.WriteLine("Reference - Match");
            foreach (BsonDocument i in dbReference.GetCollection<BsonDocument>("Match").Indexes.List().ToList())
                Console.WriteLine(i);

            Console.WriteLine("Reference - Team");
            foreach (BsonDocument i in dbReference.GetCollection<BsonDocument>("Team").Indexes.List().ToList())
                Console.WriteLine(i);

            Console.WriteLine("Embedded - Match");
            foreach (BsonDocument i in dbEmbedded.GetCollection<BsonDocument>("Match").Indexes.List().ToList())
                Console.WriteLine(i);
        }

        void CreaIndici()
        {
            Console.WriteLine("\nIndexesManager: inizio creazione indici");

            foreach (Indice i in indici)
            {
                Stopwatch watch = Stopwatch.StartNew();

                var x = new CreateIndexModel<BsonDocument>("{ '" + i.campo + "': 1 }");
                (i.database == "Reference" ? dbReference : dbEmbedded).GetCollection<BsonDocument>(i.collezione).Indexes.CreateOne(x);

                watch.Stop();
                long elapsedMs = watch.ElapsedMilliseconds;
                Console.WriteLine("\nIndice " + i.database + " - " + i.collezione + " - " + i.campo + "\nTempo(ms): " + elapsedMs);
            }

            Console.WriteLine("\nIndexesManager: fine creazione indici");
        }

        void EliminaIndici()
        {
            Console.WriteLine("\nIndexesManager: inizio eliminazione indici");
            Stopwatch watch = Stopwatch.StartNew();

            dbReference.GetCollection<BsonDocument>("Match").Indexes.DropAll();
            dbReference.GetCollection<BsonDocument>("Team").Indexes.DropAll();
            dbEmbedded.GetCollection<BsonDocument>("Match").Indexes.DropAll();

            watch.Stop();
            long elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("\nIndexesManager: fine eliminazione indici\nTempo(ms): " + elapsedMs);
        }
    }

    class Indice
    {
        public readonly string database;
        public readonly string collezione;
        public readonly string campo;

        public Indice(string db, string co, string ca)
        {
            database = db;
            collezione = co;
            campo = ca;
        }
    }
}
