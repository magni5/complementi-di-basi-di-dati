﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;

namespace CBD_MagniMarco_851810.Classes
{
    [Obsolete]
    public class PositionsManager
    {
        IMongoDatabase db;

        public PositionsManager(IMongoDatabase database)
        {
            if (database == null)
            {
                throw new ArgumentNullException("PositionsManager: database non può essere nullo");
            }
            else
            {
                db = database;
                Elabora();
            }
        }

        void Elabora()
        {
            Console.WriteLine("PositionsManager: inizio elaborazione");
            Stopwatch watch = Stopwatch.StartNew();

            IMongoCollection<BsonDocument> matchCollection = db.GetCollection<BsonDocument>("Match");

            string proiezione = "{ ";
            for (int i = 1; i <= 11; i++)
            {
                proiezione += "home_player_Y" + i + ": 1, ";
                proiezione += "away_player_Y" + i + ": 1, ";
            }
            proiezione += "_id: 1, match_api_id: 1 }";
            List<BsonDocument> matchDocs = matchCollection.Find(new BsonDocument()).Project(proiezione).ToList();

            foreach (BsonDocument matchDoc in matchDocs)
            {
                Associa(matchCollection, matchDoc);
            }

            watch.Stop();
            long elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("PositionsManager: fine elaborazione.\nTempo(ms): " + elapsedMs);
        }

        void Associa(IMongoCollection<BsonDocument> c, BsonDocument m)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("_id", m.GetElement("_id"));

            int[] posizioniHome = new int[11], posizioniAway = new int[11];
            for (int i = 1; i <= 11; i++)
            {
                posizioniHome[i-1] = m.GetElement("home_player_Y" + i).Value.ToInt32();
                posizioniAway[i-1] = m.GetElement("away_player_Y" + i).Value.ToInt32();
            }

            var update = Builders<BsonDocument>.Update
                .Set("home_team_formation", GetFormazione(posizioniHome))
                .Set("away_team_formation", GetFormazione(posizioniAway));

            c.UpdateOne(filter, update);
        }

        string GetFormazione(int[] posizioni)
        {
            try
            {
                int[] ruoli = new int[3];
                foreach (int i in posizioni)
                {
                    if (i == 0)
                        break;
                    if (i == 1)
                        continue;
                    ruoli[GetRuolo(i)]++;
                }
                return ruoli[0] + "-" + ruoli[1] + "-" + ruoli[2];
            } catch (Exception)
            {
                return "";
            }
        }
        int GetRuolo(int y)
        {
            switch (y)
            {
                case int i when i >= 1 && i <= 5: return 0;
                case int i when i >= 6 && i <= 8: return 1;
                case int i when i >= 9 && i <= 11: return 2;
                default: throw new Exception("Ruolo non esistente");
            }
        }
    }
}
