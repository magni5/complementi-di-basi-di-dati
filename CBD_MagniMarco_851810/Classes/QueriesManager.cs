﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;

namespace CBD_MagniMarco_851810.Classes
{
    public class QueriesManager
    {
        MongoClient client;
        IMongoDatabase dbReference;
        IMongoDatabase dbEmbedded;
        List<Interrogazione> interrogazioni;

        public QueriesManager(MongoClient c)
        {
            client = c;
            dbReference = client.GetDatabase("esd-reference");
            dbEmbedded = client.GetDatabase("esd-embedded");
            InizializzaInterrogazioni();
        }

        void InizializzaInterrogazioni()
        {
            interrogazioni = new List<Interrogazione>();

            #region Interrogazione 1

            interrogazioni.Add(new Interrogazione(
                "Media goal per campionato",

                () => dbReference.GetCollection<BsonDocument>("Match").Aggregate()
                    .Group(@"{
                        _id: {
                            campionato: '$league.name'
                        },
                        mediaGoal: {
                            $avg: { $sum: ['$home.goal', '$away.goal'] }
                        }
                    }")
                    .Sort(@"{
                        mediaGoal: 1
                    }")
                    .ToList(),

                () => dbEmbedded.GetCollection<BsonDocument>("Match").Aggregate()
                    .Group(@"{
                        _id: {
                            campionato: '$league.name'
                        },
                        mediaGoal: {
                            $avg: { $sum: ['$home.goal', '$away.goal'] }
                        }
                    }")
                    .Sort(@"{
                        mediaGoal: 1
                    }")
                    .ToList()
                ));

            #endregion

            #region Interrogazione 2

            interrogazioni.Add(new Interrogazione(
                "Media punti del 'FC Barcelona' con e senza 'Lionel Messi'",

                () => dbReference.GetCollection<BsonDocument>("Team").Aggregate()
                    .Match(@"{
                        'long_name': 'FC Barcelona'
                    }")
                    .Lookup(dbReference.GetCollection<BsonDocument>("Match"),
                        BsonDocument.Parse(@"{ 'fcBarcelonaCode': '$code' }"),
                        new EmptyPipelineDefinition<BsonDocument>().Match(@"{
                            $expr: {
                                $in: ['$$fcBarcelonaCode', ['$home.team_code', '$away.team_code']]
                            }
                        }"),
                        "matches")
                    .Unwind("matches")
                    .Project(@"{
                        Messi: {
                            $cond: [
                                {
                                    $or: [
                                        { $in: [ 'Lionel Messi', '$matches.home.players' ] },
                                        { $in: [ 'Lionel Messi', '$matches.away.players' ] }
                                    ]
                                },
                                'FC Barcelona con Messi',
                                'FC Barcelona senza Messi'
                            ]
                        },
                        HomeGoal: '$matches.home.goal',
                        AwayGoal: '$matches.away.goal',
                        HomeTeam: {
                            $cond: [
                                { $eq: ['$matches.home.team_code', '$code'] },
                                'FC Barcelona',
                                'Opponents'
                            ]
                        }
                    }")
                    .Group(@"{
                        _id: '$Messi',
                        totPartite: {
                            $sum: 1
                        },
                        mediaPunti: {
                            $avg: {
                                $cond: [
                                    { $eq: ['$HomeTeam', 'FC Barcelona'] },
                                    {
                                        $switch: {
                                            branches: [
                                                { case: { $gt: ['$HomeGoal', '$AwayGoal'] }, then: 3 },
                                                { case: { $eq: ['$HomeGoal', '$AwayGoal'] }, then: 1 },
                                            ],
                                            default: 0
                                        }
                                    },
                                    {
                                        $switch: {
                                            branches: [
                                                { case: { $gt: ['$AwayGoal', '$HomeGoal'] }, then: 3 },
                                                { case: { $eq: ['$AwayGoal', '$HomeGoal'] }, then: 1 },
                                            ],
                                            default: 0
                                        }
                                    }
                                ]
                            }
                        }
                    }")
                    .ToList(),

                () => dbEmbedded.GetCollection<BsonDocument>("Match").Aggregate()
                    .Match(@"{
                        $or: [
                            { 'home.team.long_name': 'FC Barcelona' },
                            { 'away.team.long_name': 'FC Barcelona' }
                        ]
                    }")
                    .Project(@"{
                        Messi: {
                            $cond: [
                                {
                                    $or: [
                                        { $in: [ 'Lionel Messi', '$home.players' ] },
                                        { $in: [ 'Lionel Messi', '$away.players' ] }
                                    ]
                                },
                                'FC Barcelona con Messi',
                                'FC Barcelona senza Messi'
                            ]
                        },
                        HomeGoal: '$home.goal',
                        AwayGoal: '$away.goal',
                        IsBarcelonaHomeTeam: {
                            $eq: ['$home.team.long_name', 'FC Barcelona']
                        }
                    }")
                    .Group(@"{
                        _id: '$Messi',
                        totPartite: {
                            $sum: 1
                        },
                        mediaPunti: {
                            $avg: {
                                $cond: [
                                    '$IsBarcelonaHomeTeam',
                                    {
                                        $switch: {
                                            branches: [
                                                { case: { $gt: ['$HomeGoal', '$AwayGoal'] }, then: 3 },
                                                { case: { $eq: ['$HomeGoal', '$AwayGoal'] }, then: 1 },
                                            ],
                                            default: 0
                                        }
                                    },
                                    {
                                        $switch: {
                                            branches: [
                                                { case: { $gt: ['$AwayGoal', '$HomeGoal'] }, then: 3 },
                                                { case: { $eq: ['$AwayGoal', '$HomeGoal'] }, then: 1 },
                                            ],
                                            default: 0
                                        }
                                    }
                                ]
                            }
                        }
                    }").ToList()
                ));

            #endregion

            #region Interrogazione 3

            interrogazioni.Add(new Interrogazione(
                "Media punti della squadra '8634' con e senza 'Lionel Messi'",

                () => dbReference.GetCollection<BsonDocument>("Match").Aggregate()
                    .Match(@"{
                        $or: [
                            { 'home.team_code': 8634 },
                            { 'away.team_code': 8634 }
                        ]
                    }")
                    .Project(@"{
                        Messi: {
                            $cond: [
                                {
                                    $or: [
                                        { $in: [ 'Lionel Messi', '$home.players' ] },
                                        { $in: [ 'Lionel Messi', '$away.players' ] }
                                    ]
                                },
                                'FC Barcelona con Messi',
                                'FC Barcelona senza Messi'
                            ]
                        },
                        HomeGoal: '$home.goal',
                        AwayGoal: '$away.goal',
                        IsBarcelonaHomeTeam: {
                            $eq: ['$home.team_code', 8634]
                        }
                    }")
                    .Group(@"{
                        _id: '$Messi',
                        totPartite: {
                            $sum: 1
                        },
                        mediaPunti: {
                            $avg: {
                                $cond: [
                                    '$IsBarcelonaHomeTeam',
                                    {
                                        $switch: {
                                            branches: [
                                                { case: { $gt: ['$HomeGoal', '$AwayGoal'] }, then: 3 },
                                                { case: { $eq: ['$HomeGoal', '$AwayGoal'] }, then: 1 },
                                            ],
                                            default: 0
                                        }
                                    },
                                    {
                                        $switch: {
                                            branches: [
                                                { case: { $gt: ['$AwayGoal', '$HomeGoal'] }, then: 3 },
                                                { case: { $eq: ['$AwayGoal', '$HomeGoal'] }, then: 1 },
                                            ],
                                            default: 0
                                        }
                                    }
                                ]
                            }
                        }
                    }").ToList(),

                () => dbEmbedded.GetCollection<BsonDocument>("Match").Aggregate()
                    .Match(@"{
                        $or: [
                            { 'home.team.code': 8634 },
                            { 'away.team.code': 8634 }
                        ]
                    }")
                    .Project(@"{
                        Messi: {
                            $cond: [
                                {
                                    $or: [
                                        { $in: [ 'Lionel Messi', '$home.players' ] },
                                        { $in: [ 'Lionel Messi', '$away.players' ] }
                                    ]
                                },
                                'FC Barcelona con Messi',
                                'FC Barcelona senza Messi'
                            ]
                        },
                        HomeGoal: '$home.goal',
                        AwayGoal: '$away.goal',
                        IsBarcelonaHomeTeam: {
                            $eq: ['$home.team.code', 8634]
                        }
                    }")
                    .Group(@"{
                        _id: '$Messi',
                        totPartite: {
                            $sum: 1
                        },
                        mediaPunti: {
                            $avg: {
                                $cond: [
                                    '$IsBarcelonaHomeTeam',
                                    {
                                        $switch: {
                                            branches: [
                                                { case: { $gt: ['$HomeGoal', '$AwayGoal'] }, then: 3 },
                                                { case: { $eq: ['$HomeGoal', '$AwayGoal'] }, then: 1 },
                                            ],
                                            default: 0
                                        }
                                    },
                                    {
                                        $switch: {
                                            branches: [
                                                { case: { $gt: ['$AwayGoal', '$HomeGoal'] }, then: 3 },
                                                { case: { $eq: ['$AwayGoal', '$HomeGoal'] }, then: 1 },
                                            ],
                                            default: 0
                                        }
                                    }
                                ]
                            }
                        }
                    }").ToList()
                ));

            #endregion

            #region Interrogazione 4

            interrogazioni.Add(new Interrogazione(
                "Statistiche '4-4-2'",

                () => dbReference.GetCollection<BsonDocument>("Match").Aggregate()
                    .Match(@"{
                        $or: [
                            { $and: [{ 'home.formation': '4-4-2' }, { 'away.formation': { $ne: '4-4-2' } }] },
                            { $and: [{ 'away.formation': '4-4-2' }, { 'home.formation': { $ne: '4-4-2' } }] }
                        ]
                    }")
                    .Project(@"{
                        HomeGoal: '$home.goal',
                        AwayGoal: '$away.goal',
                        Is442HomeTeam: {
                            $eq: ['$home.formation', '4-4-2']
                        }
                    }")
                    .Group(@"{
                        _id: 'Statistiche 4-4-2',
                        totPartite: {
                            $sum: 1
                        },
                        vittorie: {
                            $sum: {
                                $cond: [
                                    '$Is442HomeTeam',
                                    { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        },
                        pareggi: {
                            $sum:
                                { $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0] }

                        },
                        sconfitte: {
                            $sum: {
                                $cond: [
                                    '$Is442HomeTeam',
                                    { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        }
                    }")
                    .ToList(),

                () => dbEmbedded.GetCollection<BsonDocument>("Match").Aggregate()
                    .Match(@"{
                        $or: [
                            { $and: [{ 'home.formation': '4-4-2' }, { 'away.formation': { $ne: '4-4-2' } }] },
                            { $and: [{ 'away.formation': '4-4-2' }, { 'home.formation': { $ne: '4-4-2' } }] }
                        ]
                    }")
                    .Project(@"{
                        HomeGoal: '$home.goal',
                        AwayGoal: '$away.goal',
                        Is442HomeTeam: {
                            $eq: ['$home.formation', '4-4-2']
                        }
                    }")
                    .Group(@"{
                        _id: 'Statistiche 4-4-2',
                        totPartite: {
                            $sum: 1
                        },
                        vittorie: {
                            $sum: {
                                $cond: [
                                    '$Is442HomeTeam',
                                    { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        },
                        pareggi: {
                            $sum:
                                { $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0] }
                
                        },
                        sconfitte: {
                            $sum: {
                                $cond: [
                                    '$Is442HomeTeam',
                                    { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        }
                    }")
                    .ToList()
                ));

            #endregion

            #region Interrogazione 5

            interrogazioni.Add(new Interrogazione(
                "Statistiche '4-4-2' vs '4-3-3'",

                () => dbReference.GetCollection<BsonDocument>("Match").Aggregate()
                    .Match(@"{
                        $or: [
                            { $and: [{ 'home.formation': '4-4-2' }, { 'away.formation': '4-3-3' }] },
                            { $and: [{ 'away.formation': '4-4-2' }, { 'home.formation': '4-3-3' }] }
                        ]
                    }")
                    .Project(@"{
                        HomeGoal: '$home.goal',
                        AwayGoal: '$away.goal',
                        Is442HomeTeam: {
                            $eq: ['$home.formation', '4-4-2']
                        }
                    }")
                    .Group(@"{
                        _id: 'Statistiche 4-4-2 vs 4-3-3',
                        totPartite: {
                            $sum: 1
                        },
                        'vittorie 4-4-2': {
                            $sum: {
                                $cond: [
                                    '$Is442HomeTeam',
                                    { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        },
                        pareggi: {
                            $sum: {
                                $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                            }
                        },
                        'vittorie 4-3-3': {
                            $sum: {
                                $cond: [
                                    '$Is442HomeTeam',
                                    { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        }
                    }")
                    .ToList(),

                () => dbEmbedded.GetCollection<BsonDocument>("Match").Aggregate()
                    .Match(@"{
                        $or: [
                            { $and: [{ 'home.formation': '4-4-2' }, { 'away.formation': '4-3-3' }] },
                            { $and: [{ 'away.formation': '4-4-2' }, { 'home.formation': '4-3-3' }] }
                        ]
                    }")
                    .Project(@"{
                        HomeGoal: '$home.goal',
                        AwayGoal: '$away.goal',
                        Is442HomeTeam: {
                            $eq: ['$home.formation', '4-4-2']
                        }
                    }")
                    .Group(@"{
                        _id: 'Statistiche 4-4-2 vs 4-3-3',
                        totPartite: {
                            $sum: 1
                        },
                        'vittorie 4-4-2': {
                            $sum: {
                                $cond: [
                                    '$Is442HomeTeam',
                                    { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        },
                        pareggi: {
                            $sum: {
                                $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                            }
                        },
                        'vittorie 4-3-3': {
                            $sum: {
                                $cond: [
                                    '$Is442HomeTeam',
                                    { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        }
                    }")
                    .ToList()
                ));

            #endregion

            #region Interrogazione 6

            interrogazioni.Add(new Interrogazione(
                "Statistiche 'Lanci lunghi'",

                () => dbReference.GetCollection<BsonDocument>("Team").Aggregate()
                    .Match(@"{
                        'attributes.buildUpPlayPassing': 'Long'
                    }")
                    .Lookup(dbReference.GetCollection<BsonDocument>("Match"),
                        BsonDocument.Parse(@"{ 'longBuildUpPlayPassingTeamCode': '$code' }"),
                        new EmptyPipelineDefinition<BsonDocument>().Match(@"{
                            $expr: {
                                $in: ['$$longBuildUpPlayPassingTeamCode', ['$home.team_code', '$away.team_code']]
                            }
                        }"),
                        "matches")
                    .Unwind("matches")
                    .Lookup(dbReference.GetCollection<BsonDocument>("Team"),
                        BsonDocument.Parse(@"{
                            'opponentTeamCode': {
                                $cond: [
                                    { $eq: ['$code', '$matches.home.team_code'] },
                                    '$matches.away.team_code',
                                    '$matches.home.team_code'
                                ]
                            }
                        }"),
                        new EmptyPipelineDefinition<BsonDocument>().Match(@"{
                            $expr: {
                                $and: [
                                    { $eq: ['$code', '$$opponentTeamCode'] },
                                    { $ne: ['$attributes.buildUpPlayPassing', 'Long'] }
                                ]
                            }
                        }"),
                        "opponent")
                    .Unwind("opponent")
                    .Project(@"{
                        HomeGoal: '$matches.home.goal',
                        AwayGoal: '$matches.away.goal',
                        IsLongBuildUpPlayPassingHomeTeam: {
                            $eq: ['$matches.home.team_code', '$code']
                        }
                    }")
                    .Group(@"{
                        _id: 'Statistiche Lanci lunghi',
                        totPartite: {
                            $sum: 1
                        },
                        vittorie: {
                            $sum: {
                                $cond: [
                                    '$IsLongBuildUpPlayPassingHomeTeam',
                                    { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        },
                        pareggi: {
                            $sum: {
                                $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                            }
                        },
                        sconfitte: {
                            $sum: {
                                $cond: [
                                    '$IsLongBuildUpPlayPassingHomeTeam',
                                    { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        }
                    }")
                    .ToList(),

                () => dbEmbedded.GetCollection<BsonDocument>("Match").Aggregate()
                    .Match(@"{
                        $or: [
                            { $and: [{ 'home.team.attributes.buildUpPlayPassing': 'Long' }, { 'away.team.attributes.buildUpPlayPassing': { $ne: 'Long' } }] },
                            { $and: [{ 'away.team.attributes.buildUpPlayPassing': 'Long' }, { 'home.team.attributes.buildUpPlayPassing': { $ne: 'Long' } }] },
                        ]
                    }")
                    .Project(@"{
                        HomeGoal: '$home.goal',
                        AwayGoal: '$away.goal',
                        IsLongBuildUpPlayPassingHomeTeam: {
                            $eq: ['$home.team.attributes.buildUpPlayPassing', 'Long']
                        }
                    }")
                    .Group(@"{
                        _id: 'Statistiche Lanci lunghi',
                        totPartite: {
                            $sum: 1
                        },
                        vittorie: {
                            $sum: {
                                $cond: [
                                    '$IsLongBuildUpPlayPassingHomeTeam',
                                    { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        },
                        pareggi: {
                            $sum: {
                                $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                            }                
                        },
                        sconfitte: {
                            $sum: {
                                $cond: [
                                    '$IsLongBuildUpPlayPassingHomeTeam',
                                    { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        }
                    }")
                    .ToList()));

            #endregion

            #region Interrogazione 7

            interrogazioni.Add(new Interrogazione(
                "Statistiche 'Lanci lunghi' vs 'Trappola del fuorigioco'",

                () => dbReference.GetCollection<BsonDocument>("Team").Aggregate()
                    .Match(@"{
                        'attributes.buildUpPlayPassing': 'Long'
                    }")
                    .Lookup(dbReference.GetCollection<BsonDocument>("Match"),
                        BsonDocument.Parse(@"{ 'longBuildUpPlayPassingTeamCode': '$code' }"),
                        new EmptyPipelineDefinition<BsonDocument>().Match(@"{
                            $expr: {
                                $in: ['$$longBuildUpPlayPassingTeamCode', ['$home.team_code', '$away.team_code']]
                            }
                        }"),
                        "matches")
                    .Unwind("matches")
                    .Lookup(dbReference.GetCollection<BsonDocument>("Team"),
                        BsonDocument.Parse(@"{
                            'opponentTeamCode': {
                                $cond: [
                                    { $eq: ['$code', '$matches.home.team_code'] },
                                    '$matches.away.team_code',
                                    '$matches.home.team_code'
                                ]
                            }
                        }"),
                        new EmptyPipelineDefinition<BsonDocument>().Match(@"{
                            $expr: {
                                $and: [
                                    { $eq: ['$code', '$$opponentTeamCode'] },
                                    { $eq: ['$attributes.defenceDefenderLine', 'Offside Trap'] }
                                ]
                            }
                        }"),
                        "opponent")
                    .Unwind("opponent")
                    .Project(@"{
                        HomeGoal: '$matches.home.goal',
                        AwayGoal: '$matches.away.goal',
                        IsLongBuildUpPlayPassingHomeTeam: {
                            $eq: ['$matches.home.team_code', '$code']
                        }
                    }")
                    .Group(@"{
                        _id: 'Statistiche Lanci lunghi vs Trappola del fuorigioco',
                        totPartite: {
                            $sum: 1
                        },
                        'vittorie Lanci lunghi': {
                            $sum: {
                                $cond: [
                                    '$IsLongBuildUpPlayPassingHomeTeam',
                                    { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        },
                        pareggi: {
                            $sum: {
                                $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                            }
                        },
                        'vittorie Trappola del fuorigioco': {
                            $sum: {
                                $cond: [
                                    '$IsLongBuildUpPlayPassingHomeTeam',
                                    { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        }
                    }")
                    .ToList(),

                () => dbEmbedded.GetCollection<BsonDocument>("Match").Aggregate()
                    .Match(@"{
                        $or: [
                            { $and: [{ 'home.team.attributes.buildUpPlayPassing': 'Long' }, { 'away.team.attributes.defenceDefenderLine': 'Offside Trap' }] },
                            { $and: [{ 'away.team.attributes.buildUpPlayPassing': 'Long' }, { 'home.team.attributes.defenceDefenderLine': 'Offside Trap' }] }
                        ]
                    }")
                    .Project(@"{
                        HomeGoal: '$home.goal',
                        AwayGoal: '$away.goal',
                        IsLongBuildUpPlayPassingHomeTeam: {
                            $eq: ['$home.team.attributes.buildUpPlayPassing', 'Long']
                        }
                    }")
                    .Group(@"{
                        _id: 'Statistiche Lanci lunghi vs Trappola del fuorigioco',
                        totPartite: {
                            $sum: 1
                        },
                        'vittorie Lanci lunghi': {
                            $sum: {
                                $cond: [
                                    '$IsLongBuildUpPlayPassingHomeTeam',
                                    { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        },
                        pareggi: {
                            $sum: {
                                $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                            }
                        },
                        'vittorie Trappola del fuorigioco': {
                            $sum: {
                                $cond: [
                                    '$IsLongBuildUpPlayPassingHomeTeam',
                                    { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        }
                    }")
                    .ToList()));

            #endregion

            #region Interrogazione 8

            interrogazioni.Add(new Interrogazione(
                "Statistiche 'Atalanta'",

                () => dbReference.GetCollection<BsonDocument>("Team").Aggregate()
                    .Match(@"{
                        'long_name': 'Atalanta'
                    }")
                    .Lookup(dbReference.GetCollection<BsonDocument>("Match"),
                        BsonDocument.Parse(@"{ 'atalantaCode': '$code' }"),
                        new EmptyPipelineDefinition<BsonDocument>().Match(@"{
                            $expr: {
                                $in: ['$$atalantaCode', ['$home.team_code', '$away.team_code']]
                            }
                        }"),
                        "matches")
                    .Unwind("matches")
                    .Project(@"{
                        HomeGoal: '$matches.home.goal',
                        AwayGoal: '$matches.away.goal',
                        IsAtalantaHomeTeam: {
                            $eq: ['$matches.home.team_code', '$code']
                        }
                    }")
                    .Group(@"{
                        _id: 'Statistiche Atalanta',
                        totPartite: {
                            $sum: 1
                        },
                        vittorie: {
                            $sum: {
                                $cond: [
                                    '$IsAtalantaHomeTeam',
                                    { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        },
                        pareggi: {
                            $sum: {
                                $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                            }
                        },
                        sconfitte: {
                            $sum: {
                                $cond: [
                                    '$IsAtalantaHomeTeam',
                                    { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        }
                    }")
                    .ToList(),

                () => dbEmbedded.GetCollection<BsonDocument>("Match").Aggregate()
                    .Match(@"{
                        $or: [
                            { 'home.team.long_name': 'Atalanta' },
                            { 'away.team.long_name': 'Atalanta' }
                        ]
                    }")
                    .Project(@"{
                        HomeGoal: '$home.goal',
                        AwayGoal: '$away.goal',
                        IsAtalantaHomeTeam: {
                            $eq: ['$home.team.long_name', 'Atalanta']
                        }
                    }")
                    .Group(@"{
                        _id: 'Statistiche Atalanta',
                        totPartite: {
                            $sum: 1
                        },
                        vittorie: {
                            $sum: {
                                $cond: [
                                    '$IsAtalantaHomeTeam',
                                    { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        },
                        pareggi: {
                            $sum: {
                                $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                            }
                        },
                        sconfitte: {
                            $sum: {
                                $cond: [
                                    '$IsAtalantaHomeTeam',
                                    { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                                    { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                                ]
                            }
                        }
                    }")
                    .ToList()));

            #endregion

            #region Interrogazione 9

            interrogazioni.Add(new Interrogazione(
                "Numero di squadre 'Lanci Lunghi'",

                () => dbReference.GetCollection<BsonDocument>("Match").Aggregate()
                    .Match(@"{
                        'attributes.buildUpPlayPassing': 'Long'
                    }")
                    .Group(@"{
                        _id: 'Lanci lunghi',
                        numSquadre: {
                            $sum: 1
                        }
                    }")
                    .ToList(),

                () => dbEmbedded.GetCollection<BsonDocument>("Match").Aggregate()
                    .Match(@"{
                        $or: [
                            { $and: [{ 'home.team.attributes.buildUpPlayPassing': 'Long' }] },
                            { $and: [{ 'away.team.attributes.buildUpPlayPassing': 'Long' }] }
                        ]
                    }")
                    .Project(@"{
                        LongBuildUpPlayPassingTeamCode: {
                            $cond: [
                                { $eq: ['$home.team.attributes.buildUpPlayPassing', 'Long'] },
                                '$home.team.code',
                                '$away.team.code'
                            ]
                        }
                    }")
                    .Group(@"{
                        _id: 'Lanci Lunghi',
                        uniqueCount: {
                            $addToSet: '$LongBuildUpPlayPassingTeamCode'
                        }
                    }")
                    .Project(@"{
                        numSquadre: {
                            $size: '$uniqueCount'
                        }
                    }")
                    .ToList()
                ));

            #endregion
        }

        public void Elabora()
        {
            bool uscita = false;
            do
            {
                Console.WriteLine("\nMenù interrogazioni\n0) Torna al menù principale");
                for (int i = 0; i < interrogazioni.Count; i++)
                    Console.WriteLine((i + 1) + ") " + interrogazioni[i].titolo);
                Console.Write("Scelta: ");
                string input = Console.ReadLine();

                switch (input)
                {
                    case "0":
                        uscita = true;
                        break;
                    case string x when String.Compare(x, "1") >= 0 && String.Compare(x, interrogazioni.Count.ToString()) <= 0:
                        ConfiguraInterrogazione(int.Parse(input) - 1);
                        break;
                    default:
                        Console.WriteLine("Scelta non valida");
                        break;
                }
            }
            while (!uscita);
        }

        void ConfiguraInterrogazione(int indice)
        {
            bool uscita = false;
            do
            {

                Console.WriteLine("\nConfigura interrogazione\n0) Torna indietro\nR) Reference\nE) Embedded");
                Console.Write("Scelta: ");
                string input = Console.ReadLine();

                switch (input)
                {
                    case "0":
                        uscita = true;
                        break;
                    case "R":
                        EseguiInterrogazione(indice, interrogazioni[indice].EseguiInterrogazioneReference, "Reference");
                        break;
                    case "E":
                        EseguiInterrogazione(indice, interrogazioni[indice].EseguiInterrogazioneEmbedded, "Embedded");
                        break;
                    default:
                        Console.WriteLine("Scelta non valida");
                        break;
                }
            }
            while (!uscita);
        }

        void EseguiInterrogazione(int indice, Func<List<BsonDocument>> Interroga, string descrizione)
        {
            Console.WriteLine("\nQueriesManager: inizio esecuzione interrogazione " + (indice + 1) + " (" + descrizione + ")");
            Stopwatch watch = Stopwatch.StartNew();

            List<BsonDocument> risultato = Interroga();

            foreach (BsonDocument r in risultato)
                Console.WriteLine(r);
            
            watch.Stop();
            long elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("QueriesManager: fine esecuzione interrogazione\nTempo(ms): " + elapsedMs);
        }
    }

    class Interrogazione
    {
        public readonly string titolo;
        public readonly Func<List<BsonDocument>> EseguiInterrogazioneReference;
        public readonly Func<List<BsonDocument>> EseguiInterrogazioneEmbedded;

        public Interrogazione (string t, Func<List<BsonDocument>> FR, Func<List<BsonDocument>> FE)
        {
            titolo = t;
            EseguiInterrogazioneReference = FR;
            EseguiInterrogazioneEmbedded = FE;
        }
    }
}




