﻿using System;
using MongoDB.Driver;
using MongoDB.Bson;
using CBD_MagniMarco_851810.Classes;

namespace CBD_MagniMarco_851810
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Avvio main");

                MongoClient client = new MongoClient("mongodb://localhost:27017");
                //Console.WriteLine("Mi sono connesso all'istanza locale di MongoDB");

                bool uscita = false;
                do
                {
                    Console.WriteLine("\nMenù principale\n0) Termina programma\n1) Importa dati da CSV\n2) Gestione indici\n3) Esegui interrogazioni");
                    Console.Write("Scelta: ");
                    string input = Console.ReadLine();

                    switch (input)
                    {
                        case "1":
                            CsvManager csvManager = new CsvManager(client, "../../Resources/");
                            csvManager.Elabora();
                            break;
                        case "2":
                            IndexesManager indexesManager = new IndexesManager(client);
                            indexesManager.Elabora();
                            break;
                        case "3":
                            QueriesManager queriesManager = new QueriesManager(client);
                            queriesManager.Elabora();
                            break;
                        case "0":
                            uscita = true;
                            break;
                        default:
                            Console.WriteLine("Scelta non valida");
                            break;
                    }
                }
                while (!uscita);

            }
            catch (MongoException ex)
            {
                Console.WriteLine("\nErrore MongoDB: " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nErrore generico: " + ex.Message);
            }
            finally
            {
                Console.WriteLine("\nFine main");
            }
        }
    }
}