﻿db.Match.aggregate([
    {
        $group: {
            _id: {
                campionato: '$league.name'
            },
            mediaGoal: {
                $avg: { $sum: ['$home.goal', '$away.goal'] }
            }
        }
    },
    {
        $sort: {
            mediaGoal: 1
        }
    }
])