﻿db.Match.aggregate([
    {
        $match: {
            $or: [
                { 'home.team.long_name': 'FC Barcelona' },
                { 'away.team.long_name': 'FC Barcelona' }
            ]
        }
    },
    {
        $project: {
            Messi: {
                $cond: [
                    {
                        $or: [
                            { $in: ['Lionel Messi', '$home.players'] },
                            { $in: ['Lionel Messi', '$away.players'] }
                        ]
                    },
                    'FC Barcelona con Messi',
                    'FC Barcelona senza Messi'
                ]
            },
            HomeGoal: '$home.goal',
            AwayGoal: '$away.goal',
            IsBarcelonaHomeTeam: {
                $eq: ['$home.team.long_name', 'FC Barcelona']
            }
        }
    },
    {
        $group: {
            _id: '$Messi',
            totPartite: {
                $sum: 1
            },
            mediaPunti: {
                $avg: {
                    $cond: [
                        '$IsBarcelonaHomeTeam',
                        {
                            $switch: {
                                branches: [
                                    { case: { $gt: ['$HomeGoal', '$AwayGoal'] }, then: 3 },
                                    { case: { $eq: ['$HomeGoal', '$AwayGoal'] }, then: 1 },
                                ],
                                default: 0
                            }
                        },
                        {
                            $switch: {
                                branches: [
                                    { case: { $gt: ['$AwayGoal', '$HomeGoal'] }, then: 3 },
                                    { case: { $eq: ['$AwayGoal', '$HomeGoal'] }, then: 1 },
                                ],
                                default: 0
                            }
                        }
                    ]
                }
            }
        }
    }
])