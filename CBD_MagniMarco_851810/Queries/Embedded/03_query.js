﻿db.Match.aggregate([
    {
        $match: {
            $or: [
                { 'home.team.code': 8634 },
                { 'away.team.code': 8634 }
            ]
        }
    },
    {
        $project: {
            Messi: {
                $cond: [
                    {
                        $or: [
                            { $in: ['Lionel Messi', '$home.players'] },
                            { $in: ['Lionel Messi', '$away.players'] }
                        ]
                    },
                    'FC Barcelona con Messi',
                    'FC Barcelona senza Messi'
                ]
            },
            HomeGoal: '$home.goal',
            AwayGoal: '$away.goal',
            IsBarcelonaHomeTeam: {
                $eq: ['$home.team.code', 8634]
            }
        }
    },
    {
        $group: {
            _id: '$Messi',
            totPartite: {
                $sum: 1
            },
            mediaPunti: {
                $avg: {
                    $cond: [
                        '$IsBarcelonaHomeTeam',
                        {
                            $switch: {
                                branches: [
                                    { case: { $gt: ['$HomeGoal', '$AwayGoal'] }, then: 3 },
                                    { case: { $eq: ['$HomeGoal', '$AwayGoal'] }, then: 1 },
                                ],
                                default: 0
                            }
                        },
                        {
                            $switch: {
                                branches: [
                                    { case: { $gt: ['$AwayGoal', '$HomeGoal'] }, then: 3 },
                                    { case: { $eq: ['$AwayGoal', '$HomeGoal'] }, then: 1 },
                                ],
                                default: 0
                            }
                        }
                    ]
                }
            }
        }
    }
])