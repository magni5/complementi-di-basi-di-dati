﻿db.Match.aggregate([
    {
        $match: {
            $or: [
                { $and: [{ 'home.formation': '4-4-2' }, { 'away.formation': '4-3-3' }] },
                { $and: [{ 'away.formation': '4-4-2' }, { 'home.formation': '4-3-3' }] }
            ]
        }
    },
    {
        $project: {
            HomeGoal: '$home.goal',
            AwayGoal: '$away.goal',
            Is442HomeTeam: {
                $eq: ['$home.formation', '4-4-2']
            }
        }
    },
    {
        $group: {
            _id: 'Statistiche 4-4-2 vs 4-3-3',
            totPartite: {
                $sum: 1
            },
            'vittorie 4-4-2': {
                $sum: {
                    $cond: [
                        '$Is442HomeTeam',
                        { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            },
            pareggi: {
                $sum:
                    { $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0] }
                
            },
            'vittorie 4-3-3': {
                $sum: {
                    $cond: [
                        '$Is442HomeTeam',
                        { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            }
        }
    }
])