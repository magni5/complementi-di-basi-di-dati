﻿db.Match.aggregate([
    {
        $match: {
            $or: [
                { $and: [{ 'home.team.attributes.buildUpPlayPassing': 'Long' }, { 'away.team.attributes.buildUpPlayPassing': { $ne: 'Long' } }] },
                { $and: [{ 'away.team.attributes.buildUpPlayPassing': 'Long' }, { 'home.team.attributes.buildUpPlayPassing': { $ne: 'Long' } }] },
            ]
        }
    },
    {
        $project: {
            HomeGoal: '$home.goal',
            AwayGoal: '$away.goal',
            IsLongBuildUpPlayPassingHomeTeam: {
                $eq: ['$home.team.attributes.buildUpPlayPassing', 'Long']
            }
        }
    },
    {
        $group: {
            _id: 'Statistiche Lanci lunghi',
            totPartite: {
                $sum: 1
            },
            vittorie: {
                $sum: {
                    $cond: [
                        '$IsLongBuildUpPlayPassingHomeTeam',
                        { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            },
            pareggi: {
                $sum: {
                    $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                }                
            },
            sconfitte: {
                $sum: {
                    $cond: [
                        '$IsLongBuildUpPlayPassingHomeTeam',
                        { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            }
        }
    }
])