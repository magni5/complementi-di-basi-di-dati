﻿db.Match.aggregate([
    {
        $match: {
            $or: [
                { 'home.team.long_name': 'Atalanta' },
                { 'away.team.long_name': 'Atalanta' }
            ]
        }
    },
    {
        $project: {
            HomeGoal: '$home.goal',
            AwayGoal: '$away.goal',
            IsAtalantaHomeTeam: {
                $eq: ['$home.team.long_name', 'Atalanta']
            }
        }
    },
    {
        $group: {
            _id: 'Statistiche Atalanta',
            totPartite: {
                $sum: 1
            },
            vittorie: {
                $sum: {
                    $cond: [
                        '$IsAtalantaHomeTeam',
                        { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            },
            pareggi: {
                $sum: {
                    $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                }
            },
            sconfitte: {
                $sum: {
                    $cond: [
                        '$IsAtalantaHomeTeam',
                        { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            }
        }
    }
])