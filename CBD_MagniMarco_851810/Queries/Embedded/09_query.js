﻿db.Match.aggregate([
    {
        $match: {
            $or: [
                { $and: [{ 'home.team.attributes.buildUpPlayPassing': 'Long' }] },
                { $and: [{ 'away.team.attributes.buildUpPlayPassing': 'Long' }] }
            ]
        }
    },
    {
        $project: {
            LongBuildUpPlayPassingTeamCode: {
                $cond: [
                    { $eq: ['$home.team.attributes.buildUpPlayPassing', 'Long'] },
                    '$home.team.code',
                    '$away.team.code'
                ]
            }
        }
    },
    {
        $group: {
            _id: 'Lanci Lunghi',
            uniqueCount: {
                $addToSet: '$LongBuildUpPlayPassingTeamCode'
            }
        }
    },
    {
        $project: {
            numSquadre: {
                $size: '$uniqueCount'
            }
        }
    }
])