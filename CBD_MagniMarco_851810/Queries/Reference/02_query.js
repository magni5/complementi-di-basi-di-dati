﻿db.Team.aggregate([
    {
        $match: { 'long_name': 'FC Barcelona' }
    },
    {
        $lookup: {
            from: 'Match',
            let: {
                'fcBarcelonaCode': '$code'
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $in: ['$$fcBarcelonaCode', ['$home.team_code', '$away.team_code']]
                        }
                    }
                }
            ],
            as: 'matches'
        }
    },
    {
        $unwind: '$matches'
    },
    {
        $project: {
            Messi: {
                $cond: [
                    {
                        $or: [
                            { $in: ['Lionel Messi', '$matches.home.players'] },
                            { $in: ['Lionel Messi', '$matches.away.players'] }
                        ]
                    },
                    'FC Barcelona con Messi',
                    'FC Barcelona senza Messi'
                ]
            },
            HomeGoal: '$matches.home.goal',
            AwayGoal: '$matches.away.goal',
            IsBarcelonaHomeTeam: {
                $eq: ['$matches.home.team_code', '$code']
            }
        }
    },
    {
        $group: {
            _id: {
                Messi: '$Messi'
            },
            totPartite: {
                $sum: 1
            },
            mediaPunti: {
                $avg: {
                    $cond: [
                        '$IsBarcelonaHomeTeam',
                        {
                            $switch: {
                                branches: [
                                    { case: { $gt: ['$HomeGoal', '$AwayGoal'] }, then: 3 },
                                    { case: { $eq: ['$HomeGoal', '$AwayGoal'] }, then: 1 },
                                ],
                                default: 0
                            }
                        },
                        {
                            $switch: {
                                branches: [
                                    { case: { $gt: ['$AwayGoal', '$HomeGoal'] }, then: 3 },
                                    { case: { $eq: ['$AwayGoal', '$HomeGoal'] }, then: 1 },
                                ],
                                default: 0
                            }
                        }
                    ]
                }
            }
        }
    }
])


/* Versione partendo da Match */

db.Match.aggregate([
    {
        $lookup: {
            from: "Team",
            localField: "home.team_code",
            foreignField: "code",
            as: "home.team"
        }
    },
    {
        $lookup: {
            from: "Team",
            localField: "away.team_code",
            foreignField: "code",
            as: "away.team"
        }
    },
    {
        $unwind: "$home.team"
    },
    {
        $unwind: "$away.team"
    },
    {
        $match: {
            $or: [
                { 'home.team.long_name': 'FC Barcelona' },
                { 'away.team.long_name': 'FC Barcelona' }
            ]
        }
    },
    {
        $project: {
            Messi: {
                $cond: [
                    {
                        $or: [
                            { $in: ['Lionel Messi', '$home.players'] },
                            { $in: ['Lionel Messi', '$away.players'] }
                        ]
                    },
                    'con Messi',
                    'senza Messi'
                ]
            },
            HomeGoal: '$home.goal',
            AwayGoal: '$away.goal',
            HomeTeam: '$home.team.long_name',
            IsBarcelonaHomeTeam: {
                $eq: ['$home.team.long_name', 'FC Barcelona']
            }
        }
    },
    {
        $group: {
            _id: '$Messi',
            totPartite: {
                $sum: 1
            },
            mediaPunti: {
                $avg: {
                    $cond: [
                        '$IsBarcelonaHomeTeam',
                        {
                            $switch: {
                                branches: [
                                    { case: { $gt: ['$HomeGoal', '$AwayGoal'] }, then: 3 },
                                    { case: { $eq: ['$HomeGoal', '$AwayGoal'] }, then: 1 },
                                ],
                                default: 0
                            }
                        },
                        {
                            $switch: {
                                branches: [
                                    { case: { $gt: ['$AwayGoal', '$HomeGoal'] }, then: 3 },
                                    { case: { $eq: ['$AwayGoal', '$HomeGoal'] }, then: 1 },
                                ],
                                default: 0
                            }
                        }
                    ]
                }
            }
        }
    }
])