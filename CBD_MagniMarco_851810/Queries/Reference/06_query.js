﻿db.Team.aggregate([
    {
        $match: { 'attributes.buildUpPlayPassing': 'Long' }
    },
    {
        $lookup: {
            from: 'Match',
            let: {
                'longBuildUpPlayPassingTeamCode': '$code'
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $in: ['$$longBuildUpPlayPassingTeamCode', ['$home.team_code', '$away.team_code']]
                        }
                    }
                }
            ],
            as: 'matches'
        }
    },
    {
        $unwind: '$matches'
    },
    {
        $lookup: {
            from: 'Team',
            let: {
                'opponentTeamCode': {
                    $cond: [
                        { $eq: ['$code', '$matches.home.team_code'] },
                        '$matches.away.team_code',
                        '$matches.home.team_code'
                    ]
                }
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $and: [
                                { $eq: ['$code', '$$opponentTeamCode'] },
                                { $ne: ['$attributes.buildUpPlayPassing', 'Long'] }
                            ]
                        }
                    }
                }
            ],
            as: 'opponent'
        }
    },
    {
        $unwind: '$opponent'
    },
    {
        $project: {
            HomeGoal: '$matches.home.goal',
            AwayGoal: '$matches.away.goal',
            IsLongBuildUpPlayPassingHomeTeam: {
                $eq: ['$matches.home.team_code', '$code']
            }
        }
    },
    {
        $group: {
            _id: 'Statistiche Lanci lunghi',
            totPartite: {
                $sum: 1
            },
            vittorie: {
                $sum: {
                    $cond: [
                        '$IsLongBuildUpPlayPassingHomeTeam',
                        { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            },
            pareggi: {
                $sum: {
                    $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                }
            },
            sconfitte: {
                $sum: {
                    $cond: [
                        '$IsLongBuildUpPlayPassingHomeTeam',
                        { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            }
        }
    }
])