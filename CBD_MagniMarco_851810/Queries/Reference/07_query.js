﻿db.Team.aggregate([
    {
        $match: { 'attributes.buildUpPlayPassing': 'Long' }
    },
    {
        $lookup: {
            from: 'Match',
            let: {
                'longBuildUpPlayPassingTeamCode': '$code'
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $in: ['$$longBuildUpPlayPassingTeamCode', ['$home.team_code', '$away.team_code']]
                        }
                    }
                }
            ],
            as: 'matches'
        }
    },
    {
        $unwind: '$matches'
    },
    {
        $lookup: {
            from: 'Team',
            let: {
                'opponentTeamCode': {
                    $cond: [
                        { $eq: ['$code', '$matches.home.team_code'] },
                        '$matches.away.team_code',
                        '$matches.home.team_code'
                    ]
                }
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $and: [
                                { $eq: ['$code', '$$opponentTeamCode'] },
                                { $eq: ['$attributes.defenceDefenderLine', 'Offside Trap'] }
                            ]
                        }
                    }
                }
            ],
            as: 'opponent'
        }
    },
    {
        $unwind: '$opponent'
    },
    {
        $project: {
            HomeGoal: '$matches.home.goal',
            AwayGoal: '$matches.away.goal',
            IsLongBuildUpPlayPassingHomeTeam: {
                $eq: ['$matches.home.team_code', '$code']
            }
        }
    },
    {
        $group: {
            _id: 'Statistiche Lanci lunghi vs Trappola del fuorigioco',
            totPartite: {
                $sum: 1
            },
            'vittorie Lanci lunghi': {
                $sum: {
                    $cond: [
                        '$IsLongBuildUpPlayPassingHomeTeam',
                        { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            },
            pareggi: {
                $sum: {
                    $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                }
            },
            'vittorie Trappola del fuorigioco': {
                $sum: {
                    $cond: [
                        '$IsLongBuildUpPlayPassingHomeTeam',
                        { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            }
        }
    }
])


/* Versione partendo da Match */

db.Match.aggregate([
    {
        $lookup: {
            from: "Team",
            localField: "home.team_code",
            foreignField: "code",
            as: "home.team"
        }
    },
    {
        $lookup: {
            from: "Team",
            localField: "away.team_code",
            foreignField: "code",
            as: "away.team"
        }
    },
    {
        $unwind: "$home.team"
    },
    {
        $unwind: "$away.team"
    },
    {
        $match: {
            $or: [
                { $and: [{ 'home.team.attributes.buildUpPlayPassing': 'Long' }, { 'away.team.attributes.defenceDefenderLine': 'Offside Trap' }] },
                { $and: [{ 'away.team.attributes.buildUpPlayPassing': 'Long' }, { 'home.team.attributes.defenceDefenderLine': 'Offside Trap' }] }
            ]
        }
    },
    {
        $project: {
            HomeGoal: '$home.goal',
            AwayGoal: '$away.goal',
            IsLongBuildUpPlayPassingHomeTeam: {
                $eq: ['$home.team.attributes.buildUpPlayPassing', 'Long']
            }
        }
    },
    {
        $group: {
            _id: 'Statistiche Lanci lunghi vs Trappola del fuorigioco',
            totPartite: {
                $sum: 1
            },
            'vittorie Lanci lunghi': {
                $sum: {
                    $cond: [
                        '$IsLongBuildUpPlayPassingHomeTeam',
                        { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            },
            pareggi: {
                $sum: {
                    $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                }
            },
            'vittorie Trappola del fuorigioco': {
                $sum: {
                    $cond: [
                        '$IsLongBuildUpPlayPassingHomeTeam',
                        { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            }
        }
    }
])