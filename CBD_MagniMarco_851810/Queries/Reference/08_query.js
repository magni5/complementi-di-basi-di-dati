﻿db.Team.aggregate([
    {
        $match: { 'long_name': 'Atalanta' }
    },
    {
        $lookup: {
            from: 'Match',
            let: {
                'atalantaCode': '$code'
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $in: ['$$atalantaCode', ['$home.team_code', '$away.team_code']]
                        }
                    }
                }
            ],
            as: 'matches'
        }
    },
    {
        $unwind: '$matches'
    },
    {
        $project: {
            HomeGoal: '$matches.home.goal',
            AwayGoal: '$matches.away.goal',
            IsAtalantaHomeTeam: {
                $eq: ['$matches.home.team_code', '$code']
            }
        }
    },
    {
        $group: {
            _id: 'Statistiche Atalanta',
            totPartite: {
                $sum: 1
            },
            vittorie: {
                $sum: {
                    $cond: [
                        '$IsAtalantaHomeTeam',
                        { $cond: [{ $gt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $gt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            },
            pareggi: {
                $sum: {
                    $cond: [{ $eq: ['$HomeGoal', '$AwayGoal'] }, 1, 0]
                }
            },
            sconfitte: {
                $sum: {
                    $cond: [
                        '$IsAtalantaHomeTeam',
                        { $cond: [{ $lt: ['$HomeGoal', '$AwayGoal'] }, 1, 0] },
                        { $cond: [{ $lt: ['$AwayGoal', '$HomeGoal'] }, 1, 0] }
                    ]
                }
            }
        }
    }
])