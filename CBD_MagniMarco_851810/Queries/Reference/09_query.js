﻿db.Team.aggregate([
    {
        $match: {
            'attributes.buildUpPlayPassing': 'Long'
        }
    },
    {
        $group: {
            _id: 'Lanci lunghi',
            numSquadre: {
                $sum: 1
            }
        }
    }
])


/* Versione con Count */

db.Team.aggregate([
    {
        $match: {
            'attributes.buildUpPlayPassing': 'Long'
        }
    },
    {
        $count: 'Lanci lunghi'
    }
])