# Advanced data base systems

Comparison of using referenced or embedded collections in MongoDB. The dataset is related to professional football matches of the main european championships between 2008 and 2016.

Check out the [Report](Report/Report.pdf) for more.
