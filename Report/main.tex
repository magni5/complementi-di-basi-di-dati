\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[italian]{babel}
\usepackage{hyperref}

% bibliografia
\usepackage[backend=biber,style=numeric,sorting=none]{biblatex}
\usepackage{csquotes}
\addbibresource{bibliografia.bib}

% itemize
\usepackage{enumitem}
\setitemize{noitemsep,parsep=0pt,partopsep=0pt}
\setlistdepth{5}
\setlist[itemize,1]{label=$\diamond$}
\setlist[itemize,2]{label=$\bullet$}
\setlist[itemize,3]{label=$\circ$}
\setlist[itemize,4]{label=$\ast$}
\setlist[itemize,5]{label=-}
\renewlist{itemize}{itemize}{5}

% tabelle
\usepackage{changepage}
\usepackage{multirow,tabularx}

% intestazione
\title{Complementi di basi di dati}
\author{Marco Magni - 851810}
\date{Giugno 2022}

\begin{document}

\maketitle
\begin{abstract}
    Confronto tra l'utilizzo di MongoDB con collezioni strutturate con Reference e collezioni Embedded, utilizzando un dataset di dati inerenti alle partite di squadre di calcio professionistiche dei principali campionati europei tra il 2008 e il 2016.
\end{abstract}

\section{Dataset}

Il dataset utilizzato è \textit{European Soccer Database}, pubblicato su Kaggle da Hugo Mathien. \cite{esd-kaggle}

Si tratta di un archivio SQLite contente dati inerenti alle partite di squadre di calcio professionistiche dei principali campionati europei tra il 2008 e il 2016. In totale l'archivio conta più di 450.000 record e pesa $\sim$0,5 GB.

Ho scelto questo dataset, oltre per la finalità didattica del progetto, perché mi ha dato la possibilità di scoprire qualche curiosità relativa a statistiche del calcio, sport del quale sono appassionato.

\section{Importazione dei dati}

L'archivio originario conteneva le seguenti tabelle:

\begin{itemize}
    \item Country
    \item League
    \item Match
    \item Player
    \item Player$\_$Attributes
    \item Team
    \item Team$\_$Attributes
\end{itemize}

Per agevolare l'importazione dei dati in MongoDB ho deciso di esportare queste tabelle in formato CSV.

Inoltre, per semplificare il modello che ne sarebbe derivato, ho tralasciato l'importazione della relazione \textit{Player$\_$Attributes}, che non sarebbe stata oggetto delle successive interrogazioni.

Per lo stesso motivo ho rimosso le colonne della relazione \textit{Match} riguardanti informazioni di betting sportivo. Anch'esse non utili ai fini del progetto.

Altra semplificazione è stata fatta per \textit{Team$\_$Attributes}: è stato deciso di mantenere solo il record di attributi all'ultimo anno disponibile, per indurre una relazione 1:1 con \textit{Team}.

È necessario anche segnalare che durante l'importazione in MongoDB si sono usati i campi \textit{home$\_$player$\_$Y1} e similari per generare, al loro posto, il nuovo campo \textit{Formation}. In parole povere: anziché salvare le coordinate X e Y della posizione in campo di ciascun calciatore, queste vengono usate per ricavare l'informazione del modulo adottato, che sarà, in seguito, oggetto delle interrogazioni.

\section{Modellazione della base di dati}

\subsection{Reference}

Nella struttura con Reference avremo due collezioni: \textit{Match} e \textit{Team}.

Con questa soluzione si risparmierà in spazio, infatti le informazioni delle squadre verranno salvate una sola volta e poi collegate alla partita per riferimento, ma si perderà in materia di velocità di accesso alle suddette informazioni.

Questa la struttura di \textit{Match}.

\begin{itemize}
    \setlength\itemsep{0pt}
    \item Match
    \begin{itemize}
        \item $\_$id
        \item league
        \begin{itemize}
            \item name
            \item country
        \end{itemize}
        \item season
        \item stage
        \item date
        \item home
        \begin{itemize}
            \item team$\_$code
            \item goal
            \item formation
            \item players [ ]
        \end{itemize}
        \item away
        \begin{itemize}
            \item team$\_$code
            \item goal
            \item formation
            \item players [ ]
        \end{itemize}
    \end{itemize}
\end{itemize}

Questa, di conseguenza, la struttura di \textit{Team}.

\begin{itemize}
    \item Team
    \begin{itemize}
        \item $\_$id
        \item code
        \item short$\_$name
        \item long$\_$name
        \item attributes
        \begin{itemize}
            \item buildUpPlaySpeed
            \item buildUpPlayDribbling
            \item buildUpPlayPassing
            \item buildUpPlayPositioning
            \item chanceCreationPassing
            \item chanceCreationCrossing
            \item chanceCreationShooting
            \item chanceCreationPositioning
            \item defencePressure
            \item defenceAggression
            \item defenceTeamWidth
            \item defenceDefenderLine
        \end{itemize}
    \end{itemize}
\end{itemize}

\subsection{Embedded}

Nella struttura Embedded si andrà a integrare le informazioni di \textit{Team} in \textit{Match}. Chiaramente questa integrazione verrà fatta sia per la squadra di casa che per la squadra ospite.

Questa soluzione gonfierà lo spazio occupato dalla base di dati, essendo le informazioni di ogni squadra ripetute per ogni partita che avrà giocato, ma saranno notevoli i vantaggi relativi al tempo di accesso delle informazioni in questione.

Questa la collezione \textit{Match} risultante.

\begin{itemize}
    \item Match
    \begin{itemize}
        \item $\_$id
        \item league
        \begin{itemize}
            \item name
            \item country
        \end{itemize}
        \item season
        \item stage
        \item date
        \item home
        \begin{itemize}
            \item team
            \begin{itemize}
                \item code
                \item short$\_$name
                \item long$\_$name
                \item attributes
                \begin{itemize}
                    \item buildUpPlaySpeed
                    \item buildUpPlayDribbling
                    \item buildUpPlayPassing
                    \item buildUpPlayPositioning
                    \item chanceCreationPassing
                    \item chanceCreationCrossing
                    \item chanceCreationShooting
                    \item chanceCreationPositioning
                    \item defencePressure
                    \item defenceAggression
                    \item defenceTeamWidth
                    \item defenceDefenderLine
                \end{itemize}
            \end{itemize}
            \item goal
            \item formation
            \item players [ ]
        \end{itemize}
        \item away
        \begin{itemize}
            \item team
            \begin{itemize}
                \item code
                \item short$\_$name
                \item long$\_$name
                \item attributes
                \begin{itemize}
                    \item buildUpPlaySpeed
                    \item buildUpPlayDribbling
                    \item buildUpPlayPassing
                    \item buildUpPlayPositioning
                    \item chanceCreationPassing
                    \item chanceCreationCrossing
                    \item chanceCreationShooting
                    \item chanceCreationPositioning
                    \item defencePressure
                    \item defenceAggression
                    \item defenceTeamWidth
                    \item defenceDefenderLine
                \end{itemize}
            \end{itemize}
            \item goal
            \item formation
            \item players [ ]
        \end{itemize}
    \end{itemize}
\end{itemize}

\subsection{Analisi}

Nelle Tabelle \ref{tab:tempo} e \ref{tab:spazio} un'analisi del tempo impiegato per importare il dataset e dello spazio occupato dalla base di dati popolata.

\begin{table}[htp]
    \centering
    \begin{tabular}{|c|c|}
        \hline
        \multicolumn{2}{|c|}{\textbf{Tempo}} \\
        \hline
        \textbf{Reference} & \textbf{Embedded} \\
        \hline
        9.252ms & 80.997ms  \\
        \hline
    \end{tabular}
    \caption{Tempo speso per importare il dataset}
    \label{tab:tempo}
\end{table}

\begin{table}[htp]
    \centering
    \begin{tabular}{|c|c| } 
        \hline
        \multicolumn{2}{|c|}{\textbf{Tempo}} \\
        \hline
        \textbf{Reference} & \textbf{Embedded} \\
        \hline
        6,42MiB & 12,11MiB  \\
        \hline
    \end{tabular}
    \caption{Spazio occupato dall'archivio MongoDB}
    \label{tab:spazio}
\end{table}

\section{Indici}

In Tabella \ref{tab:indici} un'analisi del tempo impiegato per generare gli indici per entrambe le strutture.

\begin{table}[htp]
\begin{adjustwidth}{-4cm}{-4cm}
    \setlength\tabcolsep{3pt}
    \small
    \centering
    \begin{tabular}{|c|c|c|c|c|c|} 
        \hline
        \multicolumn{3}{|c|}{\textbf{Reference}} & \multicolumn{3}{c|}{\textbf{Embedded}} \\
        \hline
        \textbf{Collezione} & \textbf{Campo} & \textbf{Tempo} & \textbf{Collezione} & \textbf{Campo} & \textbf{Tempo} \\
        \hline
        Match & league.name & 295ms & Match & league.name & 329ms  \\
        \hline
        Team & code & 82ms & & &  \\
        Match & home.team$\_$code & 311ms & Match & home.team.code & 312ms  \\
        Match & away.team$\_$code & 304ms & Match & away.team.code & 339ms  \\
        \hline
        \multirow{2}{*}{Team} & \multirow{2}{*}{long$\_$name} & \multirow{2}{*}{90ms} & Match & home.team.long$\_$name & 329ms  \\
        & & & Match & away.team.long$\_$name & 342ms  \\
        \hline
        \multirow{2}{*}{Team} & \multirow{2}{*}{attributes.buildUpPlayPassing} & \multirow{2}{*}{66ms} & Match & home.team.attributes.buildUpPlayPassing & 333ms  \\
        & & & Match & away.team.attributes.buildUpPlayPassing & 329ms  \\
        \hline
    \end{tabular}
    \caption{Tempo speso per creare gli indici}
    \label{tab:indici}
\end{adjustwidth}
\end{table}

\section{Interrogazioni}

Per le tempistiche di esecuzione delle interrogazioni che verranno indicate vanno tenute presenti le specifiche del computer sul quale verranno eseguite, ovvero:

\begin{itemize}
    \item MacBook 2017
    \begin{itemize}
        \item Processore: 1,3 GHz Intel Core i5 dual-core
        \item Memoria RAM: 16 GB 1867 MHz LPDDR3
        \item Memoria secondaria: 256 GB NVMe SSD
    \end{itemize}
\end{itemize}

\subsection{Media gol a partita per campionato}
\label{i1}

In Tabella \ref{tab:i1} un'analisi del tempo impiegato per eseguire la prima interrogazione.

I tempi impiegati per l'esecuzione sono simili per entrambe le strutture, questo perché vengono presi in considerazione campi della sola collezione \textit{Match}, senza necessità di fare join con la collezione \textit{Team}.

Allo stesso modo i tempi non variano con l'aggiunta degli indici perché essendo di interesse della query tutti i documenti della collezione viene effettuato lo scan di tutti, indici presenti o meno.

\begin{table}[htp]
    \centering
    \begin{tabular}{|c|c|c|} 
        \cline{2-3}
        \multicolumn{1}{c|}{} & \multicolumn{2}{c|}{\textbf{Tempo}} \\
        \hline
        \textbf{Indici} & \textbf{Reference} & \textbf{Embedded} \\
        \hline
        Senza & 422ms & 438ms  \\
        \hline
        Con & 413ms & 436ms  \\
        \hline
    \end{tabular}
    \caption{Tempo per l'esecuzione della prima interrogazione}
    \label{tab:i1}
\end{table}

\subsection{Media punti del "FC Barcelona" con e senza "Lionel Messi"}
\label{i2}

In Tabella \ref{tab:i2} un'analisi del tempo impiegato per eseguire la seconda interrogazione.

Il tempo impiegato per l'esecuzione dell'interrogazione per la struttura Reference è più elevato (quasi il doppio) perché, essendo il nome esteso della squadra una proprietà dei documenti \textit{Team}, vi è la necessità di fare un join tra due collezioni.

Con l'aggiunta degli indici diventa sensibile il miglioramento per la struttura Embedded, che beneficia dell'accesso molto più veloce per le sole partite della squadra in questione.

\begin{table}[htp]
    \centering
    \begin{tabular}{|c|c|c|} 
        \cline{2-3}
        \multicolumn{1}{c|}{} & \multicolumn{2}{c|}{\textbf{Tempo}} \\
        \hline
        \textbf{Indici} & \textbf{Reference} & \textbf{Embedded} \\
        \hline
        Senza & 307ms & 163ms  \\
        \hline
        Con & 285ms & 14ms  \\
        \hline
    \end{tabular}
    \caption{Tempo per l'esecuzione della seconda interrogazione}
    \label{tab:i2}
\end{table}

\subsection{Media punti della squadra "8634" con e senza "Lionel Messi"}
\label{i3}

In Tabella \ref{tab:i3} un'analisi del tempo impiegato per eseguire la terza interrogazione.

Questa interrogazione è simile alla precedente per le informazioni estrapolate. Tuttavia, la conoscenza diretta del codice della squadra permette di evitare il join tra le due collezioni nel caso della struttura Reference, e di conseguenza i tempi tra le due strutture risulteranno simili. 

\begin{table}[htp]
    \centering
    \begin{tabular}{|c|c|c|} 
        \cline{2-3}
        \multicolumn{1}{c|}{} & \multicolumn{2}{c|}{\textbf{Tempo}} \\
        \hline
        \textbf{Indici} & \textbf{Reference} & \textbf{Embedded} \\
        \hline
        Senza & 125ms & 153ms  \\
        \hline
        Con & 19ms & 20ms  \\
        \hline
    \end{tabular}
    \caption{Tempo per l'esecuzione della terza interrogazione}
    \label{tab:i3}
\end{table}

\subsection{Statistiche "4-4-2"}
\label{i4}

In Tabella \ref{tab:i4} un'analisi del tempo impiegato per eseguire la quarta interrogazione.

I tempi impiegati per l'esecuzione sono simili per entrambe le strutture, questo perché vengono presi in considerazione campi della sola collezione \textit{Match}, senza necessità di fare join con la collezione \textit{Team}.

I tempi non vengono migliorati troppo con l'aggiunta degli indici, non essendone presenti per i campi relativi al modulo.

\begin{table}[htp]
    \centering
    \begin{tabular}{|c|c|c|} 
        \cline{2-3}
        \multicolumn{1}{c|}{} & \multicolumn{2}{c|}{\textbf{Tempo}} \\
        \hline
        \textbf{Indici} & \textbf{Reference} & \textbf{Embedded} \\
        \hline
        Senza & 248ms & 278ms  \\
        \hline
        Con & 211ms & 227ms  \\
        \hline
    \end{tabular}
    \caption{Tempo per l'esecuzione della quarta interrogazione}
    \label{tab:i4}
\end{table}

\subsection{Statistiche "4-4-2" contro "4-3-3"}
\label{i5}

In Tabella \ref{tab:i5} un'analisi del tempo impiegato per eseguire la quinta interrogazione.

Le considerazioni sono le stesse fatte per l'Interrogazione \ref{i4}. Tuttavia i tempi sono minori a causa della maggior selettività richiesta.

\begin{table}[htp]
    \centering
    \begin{tabular}{|c|c|c|} 
        \cline{2-3}
        \multicolumn{1}{c|}{} & \multicolumn{2}{c|}{\textbf{Tempo}} \\
        \hline
        \textbf{Indici} & \textbf{Reference} & \textbf{Embedded} \\
        \hline
        Senza & 163ms & 176ms  \\
        \hline
        Con & 141ms & 162ms  \\
        \hline
    \end{tabular}
    \caption{Tempo per l'esecuzione della quinta interrogazione}
    \label{tab:i5}
\end{table}

\subsection{Statistiche "Lanci lunghi"}
\label{i6}

In Tabella \ref{tab:i6} un'analisi del tempo impiegato per eseguire la sesta interrogazione.

Se per l'esecuzione dell'interrogazione per la struttura Embedded i tempi sono in linea con quelli delle interrogazioni precedenti, questi si vedono esplodere nel caso della struttura Reference. La necessità di filtrare per un attributo della collezione \textit{Team} richiede infatti una dispendiosa join.

Si nota infatti che nel caso dell'Interrogazione \ref{i2} la selettività era maggiore in quanto pari a un solo documento della collezione \textit{Team}, mentre qui è minore esistendo più squadre che adottano uno stile di gioco a "Lanci lunghi".

Gli indici vanno a dimezzare il tempo nel caso della struttura Embedded mentre non influiscono troppo nel caso Reference, in quanto utilizzati solo per la prima selezione.

\begin{table}[htp]
    \centering
    \begin{tabular}{|c|c|c|} 
        \cline{2-3}
        \multicolumn{1}{c|}{} & \multicolumn{2}{c|}{\textbf{Tempo}} \\
        \hline
        \textbf{Indici} & \textbf{Reference} & \textbf{Embedded} \\
        \hline
        Senza & 5.355ms & 184ms  \\
        \hline
        Con & 4.012ms & 72ms  \\
        \hline
    \end{tabular}
    \caption{Tempo per l'esecuzione della sesta interrogazione}
    \label{tab:i6}
\end{table}

\subsection{Statistiche "Lanci lunghi" contro "Trappola del fuorigioco"}
\label{i7}

Anche qui le considerazioni sono simili a quanto detto per l'Interrogazione \ref{i6}. L'influenza degli indici però si fa sentire maggiormente in quanto sfruttati per due selezioni.

\begin{table}[htp]
    \centering
    \begin{tabular}{|c|c|c|} 
        \cline{2-3}
        \multicolumn{1}{c|}{} & \multicolumn{2}{c|}{\textbf{Tempo}} \\
        \hline
        \textbf{Indici} & \textbf{Reference} & \textbf{Embedded} \\
        \hline
        Senza & 6.591ms & 192ms  \\
        \hline
        Con & 3.927ms & 23ms  \\
        \hline
    \end{tabular}
    \caption{Tempo per l'esecuzione della settima interrogazione}
    \label{tab:i7}
\end{table}

\subsection{Statistiche "Atalanta"}
\label{i8}

In Tabella \ref{tab:i8} un'analisi del tempo impiegato per eseguire l'ottava interrogazione.

Con questa interrogazione si vuole evidenziare come, nonostante le finalità differenti delle due, i tempi e le considerazioni sono pressoché identici a quelli dell'Interrogazione \ref{i2} (ovvero che la necessità di effettuare una join per la struttura Reference fa la differenza in termini di tempo rispetto alla più efficiente struttura Embedded).

\begin{table}[htp]
    \centering
    \begin{tabular}{|c|c|c|} 
        \cline{2-3}
        \multicolumn{1}{c|}{} & \multicolumn{2}{c|}{\textbf{Tempo}} \\
        \hline
        \textbf{Indici} & \textbf{Reference} & \textbf{Embedded} \\
        \hline
        Senza & 300ms & 154ms  \\
        \hline
        Con & 229ms & 17ms  \\
        \hline
    \end{tabular}
    \caption{Tempo per l'esecuzione dell'ottava interrogazione}
    \label{tab:i8}
\end{table}

\subsection{Numero di squadre "Lanci Lunghi"}
\label{i9}

In Tabella \ref{tab:i9} un'analisi del tempo impiegato per eseguire la nona interrogazione.

Il tempo impiegato per l'esecuzione di questa interrogazione è minore per la struttura Reference (meno della metà), in quanto le informazioni necessarie sono contenute nella sola collezione di dettaglio \textit{Team}, senza necessità di filtrare i numerosi documenti di \textit{Match}.

Vantaggio che viene annullato con l'introduzione degli indici sull'attributo in questione poiché questi consentono di accedere efficientemente alle informazioni di cui ho bisogno anche nel caso della struttura Embedded.

\begin{table}[htp]
    \centering
    \begin{tabular}{|c|c|c|} 
        \cline{2-3}
        \multicolumn{1}{c|}{} & \multicolumn{2}{c|}{\textbf{Tempo}} \\
        \hline
        \textbf{Indici} & \textbf{Reference} & \textbf{Embedded} \\
        \hline
        Senza & 74ms & 193ms  \\
        \hline
        Con & 62ms & 50ms  \\
        \hline
    \end{tabular}
    \caption{Tempo per l'esecuzione della nona interrogazione}
    \label{tab:i9}
\end{table}

\section{Applicazione}

Per la gestione della base di dati è stato realizzato un'applicativo console .NET, disponibile pubblicamente su GitLab. \cite{gitlab}

Questa applicazione agevola le operazioni di importazione dei dati, di creazione/eliminazione degli indici e di esecuzione delle interrogazioni. Tutto ciò con la possibilità di monitorare il costo in termini di tempo di ciascuna operazione.

\printbibliography

\end{document}